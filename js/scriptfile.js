var calculateButton = document.getElementById("calculate-button");
var maximumDiffInput = document.getElementById("maximum-diff-dices");

calculateButton.addEventListener("click", e => {
    calculate(parseInt(maximumDiffInput.value));
});

var template = document.querySelector(".template");

var sides = document.getElementsByClassName("template-side");

var units = [];

for (let i = 0; i < sides.length; i++) {
    let templateCloneNode = template.cloneNode(true);
    sides[i].appendChild(templateCloneNode);
    units.push(new Template(templateCloneNode));
}


let results = [document.getElementById("result-left"), document.getElementById("result-right")];
function calculate(margin=10, attackerDice=getRandomInt(0, 20), defenderDice) {
    try {
        let possibleDefenderDice = getRandomInt(attackerDice - margin - 1, attackerDice + margin);
        if (defenderDice == undefined) defenderDice = clamp(possibleDefenderDice, 1, 20);
        let infos = [
            units[0].getDamageFrom(units[1], attackerDice),
            units[1].getDamageFrom(units[0], defenderDice)
        ];


        for (let i = 0; i < infos.length; i++) {
            results[i].innerText = 
            "Opponent dice: " + infos[i]["diceNumber"] +
            "\nMax percent of damage received: " + infos[i]["maxPercentNoSupply"] + "%" +
            "\nDamage received in HP: " + infos[i]["finalDamage"] +
            "\n\nSurvivors: " + infos[i]["survivors"] + 
            "\nCasualties: " + infos[i]["casualties"] + 
            "\nWounded in action: " + infos[i]["wia"] +
            "\nMissed in action: " + infos[i]["mia"] +
            "\nKilled in action: " + infos[i]["kia"] +
            "\nMorale change: " + infos[i]["moraleChange"]
            ;
        }
    } catch (e) {
        console.log(e);
    }
}