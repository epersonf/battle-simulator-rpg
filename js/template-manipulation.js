class Template {
    constructor(node) {

        this.node = node;

        this.singleDamage = this.node.querySelector(".single-damage");
        this.soldiersAmount = this.node.querySelector(".soldiers-amount");
        this.armorClass = this.node.querySelector(".armor-class");
        this.hp = this.node.querySelector(".hp");
        this.supply = this.node.querySelector(".supply");

        this.sumDiv = this.node.querySelector(".sum-div");
        this.preparation = this.sumDiv.querySelector(".preparation");
        this.generalBonus = this.sumDiv.querySelector(".general-bonus");
        this.terrain = this.sumDiv.querySelector(".terrain");
        this.morale = this.sumDiv.querySelector(".morale");
        this.veterancy = this.sumDiv.querySelector(".veterancy");
    }

    getDamage(multiplier) {
        return parseFloat(this.singleDamage.value) * parseFloat(this.soldiersAmount.value) * multiplier;
    }

    sumSituational() {
        let sum = 0;

        for (let i = 0; i < this.sumDiv.children.length; i++) {
            if (this.sumDiv.children[i].className === "morale" || this.sumDiv.children[i].tagName != "p") continue;
            sum += parseFloat(this.sumDiv.children[i].value);
        }

        return sum;
    }

    getDamageFrom(attacker, diceNumber) {

        let mySingleDamage = parseInt(this.singleDamage.value);
        let mySoldiersAmount = parseInt(this.soldiersAmount.value);
        let myArmorClass = parseInt(this.armorClass.value);
        let mySingleHp = parseInt(this.hp.value);
        let mySupply = parseFloat(this.supply.value);
        let myMorale = parseInt(this.morale.value);
        let mySum = this.sumSituational();

        let hisSingleDamage = parseInt(attacker.singleDamage.value);
        let hisSoldiersAmount = parseInt(attacker.soldiersAmount.value);
        let hisArmorClass = parseInt(attacker.armorClass.value);
        let hisSingleHp = parseInt(attacker.hp.value);
        let hisSupply = parseFloat(attacker.supply.value);
        let hisMorale = parseInt(attacker.morale.value);
        let hisSum = attacker.sumSituational();

        if (hisMorale < 50) diceNumber = parseInt(diceNumber/(-(hisMorale-60)/10));

        let maxPercentNoSupply = calculateDamageBasedOnDice(diceNumber, myArmorClass);

        let finalDamageInPercent = clamp(hisSupply, 0, maxPercentNoSupply);

        let totalHpAmount = mySoldiersAmount * mySingleHp;

        let finalDamageInHp = clamp(attacker.getDamage(finalDamageInPercent/100), 0, totalHpAmount);

        let survivors = parseInt((totalHpAmount - finalDamageInHp)/mySingleHp);

        let moraleModifier = (myMorale - 50)/200;
        
        survivors = parseInt(clamp(
            survivors,
            mySoldiersAmount * (.7 - moraleModifier)  + getRandomInt(1, 9),
            mySoldiersAmount - getRandomInt(1, 9)
        ));

        let casualties = mySoldiersAmount - survivors;

        let moraleChange = (casualties/mySoldiersAmount) * 100;

        let kia = parseInt(casualties * (getRandomInt(-1, 20)/100));

        let mia = parseInt(casualties * (getRandomInt(-1, 1)/100));

        let wia = casualties - (kia + mia);

        
        let information = {
            "diceNumber": diceNumber,
            "maxPercentNoSupply": maxPercentNoSupply,
            "finalDamage": finalDamageInPercent,
            "survivors": survivors,
            "casualties": casualties,
            "wia": wia,
            "mia": mia,
            "kia": kia,
            "moraleChange": moraleChange.toFixed(2)
        };
        return information;
    }
}

function calculateDamageBasedOnDice(diceNumber, myArmorClass) {
    let difference = diceNumber - (myArmorClass - 5);
    let final = difference * 5;
    if (difference <= 0) {
        difference += 9;
        final = clamp(difference, 0, Infinity);
    }
    return final;
}

function clamp(n, min, max) {
    if (n <= min) return min;
    if (n >= max) return max;
    return n;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min + 1;
}